/*
 Copyright 2014 Canonical Ltd.

 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License version 3, as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranties of
 MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package service

import (
	"github.com/godbus/dbus/v5"
	. "gopkg.in/check.v1"

	"gitlab.com/ubports/development/core/lomiri-push-service/click"
)

type commonSuite struct{}

var _ = Suite(&commonSuite{})

func (cs *commonSuite) TestGrabDBusPackageAndAppIdWorks(c *C) {
	svc := new(DBusService)
	msg := &dbus.Message{
		Type: dbus.TypeMethodCall,
		Headers: map[dbus.HeaderField]dbus.Variant{
			dbus.FieldPath: dbus.MakeVariant("/com/lomiri/Postal/com_2eexample_2etest"),
		},
	}
	aPackage := "com.example.test"
	anAppId := aPackage + "_test"
	app, err := svc.grabDBusPackageAndAppId(msg, anAppId)
	c.Check(err, IsNil)
	c.Check(app.Package, Equals, aPackage)
	c.Check(app.Original(), Equals, anAppId)
}

type fakeInstalledChecker struct{}

func (fakeInstalledChecker) Installed(app *click.AppId, setVersion bool) bool {
	return app.Original()[0] == 'c'
}

func (cs *commonSuite) TestGrabDBusPackageAndAppIdFails(c *C) {
	svc := new(DBusService)
	svc.installedChecker = fakeInstalledChecker{}
	aDBusPath := "/com/lomiri/Postal/com_2eexample_2etest"
	aPackage := "com.example.test"
	anAppId := aPackage + "_test"

	for i, s := range []struct {
		path     string
		id       string
		errt     error
	}{
		{aDBusPath, aPackage, click.ErrInvalidAppId},
		{aDBusPath, "x" + anAppId, click.ErrMissingApp},
		{aDBusPath, "c" + anAppId, ErrAppIdMismatch},
	} {
		comment := Commentf("iteration #%d", i)
		msg := &dbus.Message{
			Type: dbus.TypeMethodCall,
			Headers: map[dbus.HeaderField]dbus.Variant{
				dbus.FieldPath: dbus.MakeVariant(s.path),
			},
		}
		app, err := svc.grabDBusPackageAndAppId(msg, s.id)
		c.Check(err, Equals, s.errt, comment)
		c.Check(app, IsNil, comment)
	}
}
