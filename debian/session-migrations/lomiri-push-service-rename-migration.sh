#!/bin/sh -euf

# We migrate only levels.db. helpers/ and helpers_data.json can be re-created
# by the click hook (and will likely have to be, anyway, due to app rename).
old_path="${HOME}/.local/share/ubuntu-push-client/levels.db"
new_path="${HOME}/.local/share/lomiri-push-service/levels.db"

if [ -e "$new_path" ]; then
    echo "${new_path} exists. Not migrating to avoid overwriting new information" >&2
    exit 0
fi

if ! [ -e "$old_path" ]; then
    echo "${old_path} doesn't exist. Nothing to migrate" >&2
    exit 0
fi

# Don't remove the old path, in case migration goes wrong.
mkdir -p "$(dirname "$new_path")"
cp -a "$old_path" "$new_path"

echo "Migrated lomiri-push-service seen msg and level DB" >&2
