==================
README
==================

To run lomiri-push-service autopilot tests you need to have a push server
available. This can be running locally using loopback (127.0.0.1) or remotely
on the same network.

----------------------------------
To configure and build the server:
----------------------------------

1) export GOPATH=${PWD}/push
2) mkdir -p push/src/gitlab.com/ubports/core
3) cd push/src/gitlab.com/ubports/core
4) git clone https://gitlab.com/ubports/development/core/lomiri-push-service
5) Edit lomiri-push-service/sampleconfigs/dev.json with the correct server
   IP address and ports.
   E.g. for a server IP address 192.168.1.2 use:
    "addr": "192.168.1.2:9090",
    "http_addr": "192.168.1.2:8080",
6) cd lomiri-push-service
7) Ensure all dependencies are installed: sudo apt-get build-dep lomiri-push-service
8) Install additional cm tools: sudo apt-get install git mercurial
9) make bootstrap
10) make run-server-dev
   Following output should be observed:
     INFO listening for http on 192.168.1.2:8080
     INFO listening for devices on 192.168.1.2:9090

------------------------
To configure the client:
------------------------

Install depenendencies:

1) sudo apt-get install lomiri-autopilot
2) git clone https://gitlab.com/ubports/development/core/lomiri-push-service
3) Edit ip address and ports to match environment: lomiri-push-service/tests/autopilot/push_notifications/config/push.conf:
    [config]
    addr = 192.168.1.2
    listener_port = 8080
    device_port = 9090
    cert_pem_file = testing.cert
4) systemctl stop lomiri
5) cd lomiri-push-service/tests/autopilot
6) autopilot3 list push_notifications
7) autopilot3 run push_notifications
8) To run a specific test case use the test case identifier from the list command:
     - e.g. autopilot3 run push_notifications.tests.test_broadcast_notifications.TestPushClientBroadcast.test_broadcast_push_notification

----------------
Troubleshooting:
----------------

1) Ping from client to server to ensure connectivity is correct
2) Delete ~/.local/share/lomiri-push-service/levels.db if no notifications are being displayed:
    rm ~/.local/lomiri-push-service/levels.db
3) Check client log:
    journalctl -xe lomiri-push-service
4) To send a notification manually:
     echo '{"channel":"system", "data": {"ubports-touch/16.04/stable/mako": [94, ""]}, "expire_on": "2015-12-19T16:39:57-08:00"}' | POST -c application/json http://192.168.1.2:8080/broadcast
   Response should be:
     {"ok":true}
   Note that:
     - The channel and device names must match the client.
     - The build number must be greater than current installed version in order to trigger an update message.
     - The expiration time must be in the future.
5) Ensure lomiri-autopilot is installed
6) Ensure lomiri is not running before executing the tests:
     - systemctl stop lomiri
7) Lomiri has a 2 minute timeout period, so stopping and starting it can take up to this long.
8) If device/emulator is unresponsive then reboot and stop lomiri before re-running the tests (systemctl stop lomiri).
9) To get additional autopilot logging use -v option:
     - autopilot3 run -v push_notifications
