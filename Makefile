# cgocheck=0 is a workaround for lp:1555198
GOTEST := GODEBUG=cgocheck=0 ./scripts/goctest

TOTEST = $(shell env GOPATH=$(GOPATH) go list ./...|grep -v acceptance )

all: build-client build-server

check:
	$(GOTEST) $(TESTFLAGS) $(TOTEST)

check-race:
	$(GOTEST) $(TESTFLAGS) -race $(TOTEST)

acceptance:
	cd server/acceptance && ./acceptance.sh

build-client: lomiri-push-service exec-tool

exec-tool:
	$(MAKE) -C exec-tool exec-tool

lomiri-push-service:
	go build ./cmd/$@

build-server: lomiri-push-server

run-server-dev: lomiri-push-server
	./$< sampleconfigs/dev.json

lomiri-push-server:
	go build ./cmd/$@

# very basic cleanup stuff; needs more work
clean:
	$(RM) \
	    lomiri-push-service \
	    lomiri-push-server \
	    exec-tool

distclean:
	git clean -f -x -d

coverage-summary:
	$(GOTEST) $(TESTFLAGS) -a -cover $(TOTEST)

coverage-html:
	mkdir -p coverhtml
	for pkg in $(TOTEST); do \
		relname="$${pkg#$(PROJECT)/}" ; \
		mkdir -p coverhtml/$$(dirname $${relname}) ; \
		$(GOTEST) $(TESTFLAGS) -a -coverprofile=coverhtml/$${relname}.out $$pkg ; \
		if [ -f coverhtml/$${relname}.out ] ; then \
	           go tool cover -html=coverhtml/$${relname}.out -o coverhtml/$${relname}.html ; \
	           go tool cover -func=coverhtml/$${relname}.out -o coverhtml/$${relname}.txt ; \
		fi \
	done

format:
	go fmt ./...

check-format:
	scripts/check_fmt ./...

protocol-diagrams: protocol/state-diag-client.svg protocol/state-diag-session.svg
%.svg: %.gv
	# requires graphviz installed
	dot -Tsvg $< > $@

.PHONY: acceptance all build-client build-server check check-format check-race clean coverage-html coverage-summary distclean exec-tool format lomiri-push-server lomiri-push-service protocol-diagrams run-server-dev
